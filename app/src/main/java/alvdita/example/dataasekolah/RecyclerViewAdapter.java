package alvdita.example.dataasekolah;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import alvdita.example.dataasekolah.activity.DetailActivity;
import alvdita.example.dataasekolah.model.readSiswa.ResponseReadSiswa;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    List<ResponseReadSiswa> dataReadSiswa;
    Context context;

    public RecyclerViewAdapter(Context context, ArrayList<ResponseReadSiswa> dataReadSiswa){
        this.context = context;
        this.dataReadSiswa = dataReadSiswa;
    }

    @NonNull
    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewAdapter.ViewHolder holder, int position) {

        String namaSiswa = dataReadSiswa.get(position).getNamaSiswa();
        String kelasSiswa = dataReadSiswa.get(position).getKelas();
        String idSiswa = dataReadSiswa.get(position).getIdSiswa();

        holder.txtNamaSiswa.setText(namaSiswa);
        holder.txtKelasSiswa.setText(kelasSiswa);

        holder.itemView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                intent.putExtra(DetailActivity.KEY_NAMA_SISWA, namaSiswa);
                intent.putExtra(DetailActivity.KEY_KELAS_SISWA, kelasSiswa);
                intent.putExtra(DetailActivity.KEY_ID,idSiswa);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() { return dataReadSiswa.size(); }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtNamaSiswa,txtKelasSiswa;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            txtNamaSiswa = itemView.findViewById(R.id.txtNamaSiswa);
            txtKelasSiswa = itemView.findViewById(R.id.txtListKelasSiswa);
        }
    }
}
