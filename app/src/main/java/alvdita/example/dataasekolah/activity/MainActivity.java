package alvdita.example.dataasekolah.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import alvdita.example.dataasekolah.R;
import alvdita.example.dataasekolah.RecyclerViewAdapter;
import alvdita.example.dataasekolah.model.readSiswa.ResponseReadSiswa;
import alvdita.example.dataasekolah.network.ApiClient;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    ArrayList<ResponseReadSiswa> data = null;

    @BindView(R.id.rcView)
    RecyclerView rcView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getReadSiswa();

    }

    private void getReadSiswa() {
        ApiClient.service.actionReadSiswa().enqueue(new Callback<ArrayList<ResponseReadSiswa>>() {
            @Override
            public void onResponse(Call<ArrayList<ResponseReadSiswa>> call, Response<ArrayList<ResponseReadSiswa>> response) {
                if (response.code() == 200) {
                    data = response.body();
                    Log.d("data index siswa", "" + data);
                    if (data == null) {
                        Toast.makeText(MainActivity.this, "Data Null", Toast.LENGTH_SHORT).show();
                    } else {
                        rcView.setHasFixedSize(true);
                        rcView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
                        rcView.setAdapter(new RecyclerViewAdapter(MainActivity.this, data));
                        Toast.makeText(MainActivity.this, "berhasil", Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ResponseReadSiswa>> call, Throwable t) {
                Toast.makeText(MainActivity.this, "onfailure", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()){
            case R.id.item_tambah:
                startActivity(new Intent(MainActivity.this,AddActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
