package alvdita.example.dataasekolah.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import alvdita.example.dataasekolah.R;
import alvdita.example.dataasekolah.model.add.ResponseAdd;
import alvdita.example.dataasekolah.network.ApiClient;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.edtNamaSiswaTambah)
    EditText edtNamaSiswa;
    @BindView(R.id.edtKelasSiswaTambah)
    EditText edtKelasSiswa;
    @BindView(R.id.btnTambah)
    Button btnUpdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btnTambah)
    public void onViewClicked() {

        String nama = edtNamaSiswa.getText().toString();
        String kelas = edtKelasSiswa.getText().toString();

        if (TextUtils.isEmpty(nama)){
            edtNamaSiswa.setError("Jangan Kosong");
        }else if(TextUtils.isEmpty(kelas)){
            edtKelasSiswa.setError("Jangan Kosong");
        }

        tambahData(nama,kelas);

    }

    private void tambahData(String nama, String kelas) {
        ApiClient.service.actionAdd(nama,kelas).enqueue(new Callback<ResponseAdd>() {
            @Override
            public void onResponse(Call<ResponseAdd> call, Response<ResponseAdd> response) {
                if (response.isSuccessful()){
                    String message = response.body().getMessage();
                    int status = response.body().getStatus();

                    if (status == 1){
                        Toast.makeText(AddActivity.this, message, Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(AddActivity.this,MainActivity.class));
                        finish();
                    }else if (status == 0){
                        Toast.makeText(AddActivity.this, message, Toast.LENGTH_SHORT).show();
                    }
                }

            }

            @Override
            public void onFailure(Call<ResponseAdd> call, Throwable t) {

            }
        });
    }
}
